package tungts.hust.edu.basicmaterials.coordinatorlayout.behaviors;

import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import tungts.hust.edu.basicmaterials.R;

public class BehaviorWithBottomNavigationBar extends AppCompatActivity {

    FloatingActionButton fab;
    NestedScrollView nested_scroll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_behavior_with_bottom_navigation_bar);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar snackbar = Snackbar.make(v, "This is snackbar", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        });

        nested_scroll = findViewById(R.id.nested_scroll);
        nested_scroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                String TAG = "scroll";
                if (scrollY > oldScrollY) {
                    Log.i(TAG, "Scroll DOWN");
                    fab.hide();
                }

                if (scrollY < oldScrollY) {
                    Log.i(TAG, "Scroll UP");
                    fab.show();
                }

                if (scrollY == 0) {
                    Log.i(TAG, "TOP SCROLL");
                }

                if (scrollY == ( v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight() )) {
                    Log.i(TAG, "BOTTOM SCROLL");
                }
            }

        });

    }
}
