package tungts.hust.edu.basicmaterials;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import tungts.hust.edu.basicmaterials.coordinatorlayout.CoordinatorDemo1;
import tungts.hust.edu.basicmaterials.coordinatorlayout.CoordinatorDemo2;
import tungts.hust.edu.basicmaterials.coordinatorlayout.behaviors.BehaviorFrabAndSnackBar;
import tungts.hust.edu.basicmaterials.coordinatorlayout.behaviors.BehaviorOfView;
import tungts.hust.edu.basicmaterials.coordinatorlayout.behaviors.BehaviorTouch;
import tungts.hust.edu.basicmaterials.coordinatorlayout.behaviors.BehaviorWithBottomNavigationBar;
import tungts.hust.edu.basicmaterials.drawerlayout.DrawerLayoutDemo;
import tungts.hust.edu.basicmaterials.recycleview.dragandswipe.DragAndSwipeDemo;
import tungts.hust.edu.basicmaterials.recycleview.toolbaractionmode.RecycleViewActivity;
import tungts.hust.edu.basicmaterials.recycleview.snaphelper.SnapHelperDemo;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tv_coordinator_example;
    TextView tv_coordinator_example2;
    TextView tv_behavior_touch;
    TextView tv_behavior_fab;
    TextView tv_behavior_of_view;
    TextView tv_bottom_navi;
    TextView tv_drawer_layout;
    TextView tv_demo_rcv;
    TextView tv_demo_snap_helper;
    TextView tv_demo_drag_and_swipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv_coordinator_example = findViewById(R.id.tv1);
        tv_coordinator_example.setOnClickListener(this);
        tv_coordinator_example2 = findViewById(R.id.tv2);
        tv_coordinator_example2.setOnClickListener(this);
        tv_behavior_touch = findViewById(R.id.tv3);
        tv_behavior_touch.setOnClickListener(this);
        tv_behavior_fab = findViewById(R.id.tv4);
        tv_behavior_fab.setOnClickListener(this);
        tv_bottom_navi = findViewById(R.id.tv5);
        tv_bottom_navi.setOnClickListener(this);
        tv_drawer_layout = findViewById(R.id.tv6);
        tv_drawer_layout.setOnClickListener(this);
        tv_demo_rcv = findViewById(R.id.tv7);
        tv_demo_rcv.setOnClickListener(this);
        tv_demo_snap_helper = findViewById(R.id.tv8);
        tv_demo_snap_helper.setOnClickListener(this);
        tv_demo_drag_and_swipe = findViewById(R.id.tv9);
        tv_demo_drag_and_swipe.setOnClickListener(this);
        tv_behavior_of_view = findViewById(R.id.tv10);
        tv_behavior_of_view.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv1:
                startActivity(new Intent(MainActivity.this, CoordinatorDemo1.class));
                break;
            case R.id.tv2:
                startActivity(new Intent(MainActivity.this, CoordinatorDemo2.class));
                break;
            case R.id.tv3:
                startActivity(new Intent(MainActivity.this, BehaviorTouch.class));
                break;
            case R.id.tv4:
                startActivity(new Intent(MainActivity.this, BehaviorFrabAndSnackBar.class));
                break;
            case R.id.tv5:
                startActivity(new Intent(MainActivity.this, BehaviorWithBottomNavigationBar.class));
                break;
            case R.id.tv6:
                startActivity(new Intent(MainActivity.this, DrawerLayoutDemo.class));
                break;
            case R.id.tv7:
                startActivity(new Intent(MainActivity.this, RecycleViewActivity.class));
                break;
            case R.id.tv8:
                startActivity(new Intent(MainActivity.this, SnapHelperDemo.class));
                break;
            case R.id.tv9:
                startActivity(new Intent(MainActivity.this, DragAndSwipeDemo.class));
                break;
            case R.id.tv10:
                startActivity(new Intent(MainActivity.this, BehaviorOfView.class));
                break;
        }
    }
}
