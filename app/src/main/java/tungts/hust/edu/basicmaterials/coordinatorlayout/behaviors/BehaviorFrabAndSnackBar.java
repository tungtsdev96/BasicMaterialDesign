package tungts.hust.edu.basicmaterials.coordinatorlayout.behaviors;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import tungts.hust.edu.basicmaterials.R;

public class BehaviorFrabAndSnackBar extends AppCompatActivity {

    TextView tv_touch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_behavior_touch);
        tv_touch = findViewById(R.id.tv_touch);
        ((CoordinatorLayout.LayoutParams)tv_touch.getLayoutParams()).setBehavior(null);
        tv_touch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar snackbar = Snackbar.make(v, "This is snack bar", Snackbar.LENGTH_SHORT);
                snackbar.show();
            }
        });
    }
}
