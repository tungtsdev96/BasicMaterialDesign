package tungts.hust.edu.basicmaterials.recycleview.toolbaractionmode;

public interface RecycleViewItemClick {

    void itemClick(int pos);
    void itemLongClick(int pos);

}
