package tungts.hust.edu.basicmaterials.recycleview.toolbaractionmode;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.SparseBooleanArray;
import android.widget.Toast;

import java.util.ArrayList;

import tungts.hust.edu.basicmaterials.R;

public class RecycleViewActivity extends AppCompatActivity implements RecycleViewItemClick {

    RecyclerView rcv;
    AdapterItem adapterItem;
    ArrayList list;
    private ActionMode mActionMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toolbar_action_mode);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("RecycleView");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        innitRcv();
    }

    private void innitRcv() {
        rcv = findViewById(R.id.rcv);
        list = new ArrayList();
        for  (int i = 0; i < 10; i++){
            list.add(new Item(i + 1, "Value: " + (i + 1)));
        }
        adapterItem = new AdapterItem(list,this, rcv);
        rcv.setLayoutManager(new LinearLayoutManager(this));
        rcv.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        rcv.setAdapter(adapterItem);
        adapterItem.setOnRecycleViewItemClick(this);
    }

    @Override
    public void itemClick(int pos) {
        if (mActionMode != null){
            onListItemSelected(pos);
            return;
        }
        Toast.makeText(this, "click " + pos, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void itemLongClick(int pos) {
        onListItemSelected(pos);
    }

    /*
        The functions for toolbar action mode
    */

    private void onListItemSelected(int pos) {
        boolean hasCheckedItem = adapterItem.getSelectedCount() > 0;
        adapterItem.toggleSelection(pos);

        //start action mode
        if (!hasCheckedItem && mActionMode == null) {
            mActionMode = startSupportActionMode(new ActionModeCallBack(this));
            mActionMode.setTitle("1 selected");
        } else {
            mActionMode.setTitle(String.valueOf(adapterItem
                    .getSelectedCount()) + " selected");
        }

    }

    //Set action mode null after use
    public void setNullToActionMode() {
        if (mActionMode != null)
            mActionMode = null;
    }

    //delete all item selected
    public void deleteAllItemSelected() {
        SparseBooleanArray selected = adapterItem.getSelectedIds();
        Toast.makeText(this, "delete " + selected.size() + "item" , Toast.LENGTH_SHORT).show();
    }

    //select all item
    public void selectAllItem() {
        Toast.makeText(this, "select all", Toast.LENGTH_SHORT).show();
//        adapterItem.selectionAllItem();
    }

    //remove all select
    public void removeAllSelected() {
        adapterItem.removeSelection();
    }

    /*

    */


}
