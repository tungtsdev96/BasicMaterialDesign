package tungts.hust.edu.basicmaterials.drawerlayout.frag;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import tungts.hust.edu.basicmaterials.R;

public class Frag4 extends Fragment {

    public static String TAG = "frag4";

    public static Frag4 newInstance(){
        return new Frag4();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag4, container, false);
    }

}
