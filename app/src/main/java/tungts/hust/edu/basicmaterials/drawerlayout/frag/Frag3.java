package tungts.hust.edu.basicmaterials.drawerlayout.frag;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import tungts.hust.edu.basicmaterials.R;

public class Frag3 extends Fragment {

    public static String TAG = "frag3";

    public static Frag3 newInstance(){
        return new Frag3();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag3, container, false);
    }

}
