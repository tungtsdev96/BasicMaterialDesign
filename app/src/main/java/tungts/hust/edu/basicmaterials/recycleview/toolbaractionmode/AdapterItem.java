package tungts.hust.edu.basicmaterials.recycleview.toolbaractionmode;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import tungts.hust.edu.basicmaterials.R;

public class AdapterItem extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public final int ITEM_LOAD = 99;
    public final int ITEM_ROW = 100;

    ArrayList list;
    Context context;
    LayoutInflater layoutInflater;
    RecyclerView rcv;
    private SparseBooleanArray mSelectedItemsIds;

    public AdapterItem(ArrayList list, Context context, RecyclerView rcv) {
        this.list = list;
        this.context = context;
        this.rcv = rcv;
        layoutInflater = LayoutInflater.from(context);
        mSelectedItemsIds = new SparseBooleanArray();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == ITEM_ROW){
            return new ItemViewHolder(layoutInflater.inflate(R.layout.row_item,null, false));
        }
        return new LoadViewHolder(layoutInflater.inflate(R.layout.item_load_more, null, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ItemViewHolder){
            Item item = (Item) list.get(position);
            ((ItemViewHolder) holder).tv_id.setText(item.getId()+"");
            ((ItemViewHolder) holder).tv_value.setText(item.getValue()+"");
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onRecycleViewItemClick != null){
                        onRecycleViewItemClick.itemClick(position);
                    }
                }
            });

            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (onRecycleViewItemClick != null){
                        onRecycleViewItemClick.itemLongClick(position);
                    }
                    return true;
                }
            });

            /** Change background color of the selected items in list view  **/
            holder.itemView
                    .setBackgroundColor(mSelectedItemsIds.get(position) ? 0x9934B5E4
                            : Color.TRANSPARENT);

        } else if (holder instanceof LoadViewHolder) {
            ((LoadViewHolder) holder).progress_bar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position) instanceof Item)
            return ITEM_ROW;
        return ITEM_LOAD;
    }

    protected class ItemViewHolder extends RecyclerView.ViewHolder{

        TextView tv_id, tv_value;

        public ItemViewHolder(View itemView) {
            super(itemView);
            tv_id = itemView.findViewById(R.id.tv_id);
            tv_value = itemView.findViewById(R.id.tv_value);

        }
    }

    protected class LoadViewHolder extends RecyclerView.ViewHolder{

        ProgressBar progress_bar;

        public LoadViewHolder(View itemView) {
            super(itemView);
            progress_bar = itemView.findViewById(R.id.progress_bar);
        }
    }

    RecycleViewItemClick onRecycleViewItemClick;

    public void setOnRecycleViewItemClick(RecycleViewItemClick onRecycleViewItemClick) {
        this.onRecycleViewItemClick = onRecycleViewItemClick;
    }

    /***
     * Methods required for do selections, remove selections, etc.
     */
    //Toggle selection methods
    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }


    //Remove selected selections
    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }

    //Put or delete selected position into SparseBooleanArray
    public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);
        else
            mSelectedItemsIds.delete(position);
        notifyDataSetChanged();
    }

    //Get total selected count
    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    //Return all selected ids
    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

    //Select all item
    public void selectionAllItem(){
//        for (int i = 0; i < list.size(); i++){
//            if (!mSelectedItemsIds.get(i)) selectView(i, true);
//        }
    }
}
