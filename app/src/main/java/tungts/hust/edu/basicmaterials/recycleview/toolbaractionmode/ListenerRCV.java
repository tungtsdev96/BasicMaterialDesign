package tungts.hust.edu.basicmaterials.recycleview.toolbaractionmode;

public class ListenerRCV {

    public interface RecycleItemClick{
        void onItemClick(int pos);
        void onItemLongClick(int pos);
    }

    public interface LoadMore{
        void onLoadMore();
    }

}
