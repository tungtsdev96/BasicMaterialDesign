package tungts.hust.edu.basicmaterials.recycleview.snaphelper;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;

import tungts.hust.edu.basicmaterials.R;

public class SnapHelperDemo extends AppCompatActivity {

    RecyclerView centerSnapRecyclerView;
    AppListAdapter appListCenterAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snap_helper_demo);
        centerSnapRecyclerView = findViewById(R.id.rcv);
        setUpRcv();
    }

    private void setUpRcv() {
        LinearLayoutManager layoutManagerCenter
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        centerSnapRecyclerView.setLayoutManager(layoutManagerCenter);
        appListCenterAdapter = new AppListAdapter(this);
        centerSnapRecyclerView.setAdapter(appListCenterAdapter);
        SnapHelper startSnapHelper = new StartSnapHelper();
        startSnapHelper.attachToRecyclerView(centerSnapRecyclerView);

//        LinearLayoutManager layoutManagerStart
//                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
//        startSnapRecyclerView.setLayoutManager(layoutManagerStart);
//        appListStartAdapter = new AppListAdapter(this);
//        startSnapRecyclerView.setAdapter(appListStartAdapter);
//        SnapHelper snapHelperStart = new StartSnapHelper();
//        snapHelperStart.attachToRecyclerView(startSnapRecyclerView);
    }
}
