package tungts.hust.edu.basicmaterials.drawerlayout;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.FrameLayout;

import java.util.Stack;

import tungts.hust.edu.basicmaterials.R;
import tungts.hust.edu.basicmaterials.drawerlayout.frag.Frag1;
import tungts.hust.edu.basicmaterials.drawerlayout.frag.Frag2;
import tungts.hust.edu.basicmaterials.drawerlayout.frag.Frag3;
import tungts.hust.edu.basicmaterials.drawerlayout.frag.Frag4;

public class DrawerLayoutDemo extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    Toolbar toolbar;
    private DrawerLayout drawer;
    private NavigationView navigationView;

    FrameLayout container;

    // index to identify current nav menu item
    public static int navItemIndex = 0;

    // flag to load home fragment when user presses back key
    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;
    public static String CURRENT_TAG = Frag1.TAG;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer_layout_demo);
        mHandler = new Handler();

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        container = findViewById(R.id.container);
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.navigationView);
        setUpDrawerLayout();

    }

    private Fragment getFragment() {
        switch (navItemIndex){
            case 0:
                return Frag1.newInstance();
            case 1:
                return Frag2.newInstance();
            case 2:
                return Frag3.newInstance();
            default:
                return Frag4.newInstance();
        }
    }

    private void setUpDrawerLayout() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.open, R.string.close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.navi_1:
                navItemIndex = 0;
                CURRENT_TAG = Frag1.TAG;
                loadFragment();
                break;
            case R.id.navi_2:
                navItemIndex = 1;
                CURRENT_TAG = Frag2.TAG;
                loadFragment();
                break;
            case R.id.navi_3:
                navItemIndex = 2;
                CURRENT_TAG = Frag3.TAG;
                break;
            case R.id.navi_4:
                navItemIndex = 3;
                CURRENT_TAG = Frag4.TAG;
                loadFragment();
                break;
        }
        loadFragment();
        return true;
    }

    private void loadFragment() {
        // selecting appropriate nav menu item
//        navigationView.getMenu().getItem(navItemIndex).setChecked(true);

        //setToolbar Title

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(CURRENT_TAG);

        if (fragment != null){
            Log.e("frag","not null");
            drawer.closeDrawers();
            replaceFragment(fragment);
            // show or hide the fab button
            return;
        }

        Log.e("frag","null");
        fragment = getFragment();
        replaceFragment(fragment);
        //Closing drawer on item click
        drawer.closeDrawers();
    }

    private void replaceFragment(final Fragment fragment){
        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
//                Fragment fragment = getFragment();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                ft.replace(R.id.container, fragment, CURRENT_TAG);
                ft.commitAllowingStateLoss();
            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.postDelayed(mPendingRunnable, 350);
        }
    }

    @Override
    public void onBackPressed() {
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawers();
//            return;
//        }
//
//        // This code loads home fragment when back key is pressed
//        // when user is in other fragment than home
//        if (shouldLoadHomeFragOnBackPress){
//            // checking if user is on other navigation menu
//            // rather than home
//            if (navItemIndex != 0){
//                navItemIndex = 0;
//                CURRENT_TAG = Frag1.TAG;
//                loadFragment();
//                return;
//            }
//        }


        super.onBackPressed();
    }

}
