package tungts.hust.edu.basicmaterials.coordinatorlayout.behaviors;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.View;

import de.hdodenhof.circleimageview.CircleImageView;
import tungts.hust.edu.basicmaterials.R;

/**
 * Class này được dùng để định nghĩa sự phụ thuộc giữa [CircleImageView] và [Toolbar]
 * bên trong cùng một CoordinatorLayout
 */
@SuppressWarnings("unused")
public class CustomBehaviorView extends CoordinatorLayout.Behavior<CircleImageView> {

    private final static float MIN_AVATAR_PERCENTAGE_SIZE = 0.3f;
    private final static int EXTRA_FINAL_AVATAR_PADDING = 80;

    private final static String TAG = "behavior";
    private Context mContext;

    private float mCustomFinalYPosition;
    private float mCustomStartXPosition;
    private float mCustomStartToolbarPosition;
    private float mCustomStartHeight;
    private float mCustomFinalHeight;

    private float mAvatarMaxSize;
    private float mFinalLeftAvatarPadding;
    private float mStartPosition;
    private int mStartXPosition;
    private float mStartToolbarPosition;
    private int mStartYPosition;
    private int mFinalYPosition;
    private int mStartHeight;
    private int mFinalXPosition;
    private float mChangeBehaviorPoint;

    public CustomBehaviorView(Context context, AttributeSet attrs) {
        mContext = context;

        if (attrs != null) {
            /*
            Dăm ba mấy thứ vớ vẩn này lấy từ activity_main.xml L149-L155
             */
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.AvatarImageBehavior);
            mCustomFinalYPosition = a.getDimension(R.styleable.AvatarImageBehavior_finalYPosition, 0);
            mCustomStartXPosition = a.getDimension(R.styleable.AvatarImageBehavior_startXPosition, 0);
            mCustomStartToolbarPosition = a.getDimension(R.styleable.AvatarImageBehavior_startToolbarPosition, 0);
            mCustomStartHeight = a.getDimension(R.styleable.AvatarImageBehavior_startHeight, 0);
            mCustomFinalHeight = a.getDimension(R.styleable.AvatarImageBehavior_finalHeight, 0);

            a.recycle();
        }

        init();

        mFinalLeftAvatarPadding = context.getResources().getDimension(
                R.dimen.spacing_normal);
    }

    private void init() {
        bindDimensions();
    }

    private void bindDimensions() {
        mAvatarMaxSize = mContext.getResources().getDimension(R.dimen.image_width);
    }

    /**
     * Hàm này kiểm tra sự phụ thuộc của [dependency] với các view bên trong CoordinatorLayout.
     * Túm váy lại hiểu nôm na nó duyệt đủ hết view con trong CoordinatorLayout rồi thấy thằng nào
     * thoả mãn thì dùng cái behavior này
     *
     * @param parent     chắc cmn chắn là cái CoordinatorLayout bao bọc bên ngoài cái view đang có
     *                   cái behavior này
     * @param child      là cái view đang có cái behavior này và chắc cmn chắn thằng này là
     *                   CircleImageView
     * @param dependency view con bên trong CoordinatorLayout
     * @return trường hợp này chỉ áp dụng với view nào là toolbar bên trong CoordinatorLayo
     */
    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, CircleImageView child, View dependency) {
        return dependency instanceof Toolbar;
    }

    /**
     * Khi cái thằng toolbar thay đổi, hàm này được gọi để update thằng child (CIV) khi cái thằng
     * toolbar thay đổi vị trí hoặc layout...
     */
    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, CircleImageView child, View dependency) {
        maybeInitProperties(child, dependency);

        // quãng đường dài nhất có thể kéo chính là điểm đáy của tool bar khi mới bắt đầu vào
        // activity
        final int maxScrollDistance = (int) (mStartToolbarPosition);
        // tỉ lệ giữa điểm hiện tại của cái toolbar và cái biến bên trên (tóm lại thằng này dùng
        // để phóng to thu nhỏ cái ảnh một cách hợp lí)
        float expandedPercentageFactor = dependency.getY() / maxScrollDistance;

        // Nguyên lí tính toán:
        // 1. đáy Toolbar thay đổi theo phương trình y = k (k thay đổi) k trong khoảng từ chiều
        // cao của Toolbar tới vị trí ban đầu của Toolbar ví dụ [56, 300]
        // 2. tâm của CIV thay đổi theo phương trình đường thẳng y = ak + b. Tìm a,b bằng cách vẽ
        // hình
        // 3. bán kính của CIV thay đổi theo phương trình R = ck. Tìm c
        //Trường hợp kéo lên
        if (expandedPercentageFactor < mChangeBehaviorPoint) {
            float heightFactor = (mChangeBehaviorPoint - expandedPercentageFactor) / mChangeBehaviorPoint;

            float distanceXToSubtract = ((mStartXPosition - mFinalXPosition)
                    * heightFactor) + (child.getHeight() / 2);
            float distanceYToSubtract = ((mStartYPosition - mFinalYPosition)
                    * (1f - expandedPercentageFactor)) + (child.getHeight() / 2);

            child.setX(mStartXPosition - distanceXToSubtract);
            child.setY(mStartYPosition - distanceYToSubtract);

            float heightToSubtract = ((mStartHeight - mCustomFinalHeight) * heightFactor);

            CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) child.getLayoutParams();
            lp.width = (int) (mStartHeight - heightToSubtract);
            lp.height = (int) (mStartHeight - heightToSubtract);
            child.setLayoutParams(lp);
        } else {
            // kéo xuống
            float distanceYToSubtract = ((mStartYPosition - mFinalYPosition)
                    * (1f - expandedPercentageFactor)) + (mStartHeight / 2);

            child.setX(mStartXPosition - child.getWidth() / 2);
            child.setY(mStartYPosition - distanceYToSubtract);

            CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) child.getLayoutParams();
            lp.width = (int) (mStartHeight);
            lp.height = (int) (mStartHeight);
            child.setLayoutParams(lp);
        }
        return true;
    }

    /**
     * Ban đầu mấy cái biến xàm xí là 0 nên gọi thằng này để khởi tạo, đây là lí do kotlin ngon
     * hơn java
     *
     * @param child      CIV
     * @param dependency Toolbar
     */
    private void maybeInitProperties(CircleImageView child, View dependency) {
        // điểm xuất phát theo trục y là đáy của thằng toolbar, thằng toolbar này phụ thuộc vào
        // một cái FrameLayout nằm trong CollapsingToolBar, lúc ẩn lúc hiện nên cần thiết có cái
        // thằng Behavior này để bảo cái ảnh chạy đi đâu
        if (mStartYPosition == 0)
            mStartYPosition = (int) (dependency.getY());

        // điểm cuối cùng theo chiều y là nửa chiều cao của thằng toolbar, nhìn cái hình
        // animation là hiểu thằng này muốn gì
        if (mFinalYPosition == 0)
            mFinalYPosition = (dependency.getHeight() / 2);

        // chiều cao ban đầu chắc chắn là chiều cao của cái ảnh
        if (mStartHeight == 0)
            mStartHeight = child.getHeight();

        // điểm xuất phát theo trục x là điểm chính giữa cái ảnh
        if (mStartXPosition == 0)
            mStartXPosition = (int) (child.getX() + (child.getWidth() / 2));

        // điểm cuối cùng theo chiều x là 24dp (padding) + 32dp/2 (nửa chiều rộng cái ảnh thu bé)
        if (mFinalXPosition == 0)
            mFinalXPosition = mContext.getResources().getDimensionPixelOffset(R.dimen.abc_action_bar_content_inset_material) + ((int) mCustomFinalHeight / 2);

        // Cái này ez quá rồi
        if (mStartToolbarPosition == 0)
            mStartToolbarPosition = dependency.getY();

        // tỉ lệ giữa độ lệch chiều cao và 2 lần độ lệch quãng đường??? Tại sao lại 2 ư??? Đếu
        // hiểu, nhưng bỏ 2* đi thì vẫn chạy như thường, alo???
        if (mChangeBehaviorPoint == 0) {
            mChangeBehaviorPoint = (child.getHeight() - mCustomFinalHeight) / (2 * (mStartYPosition - mFinalYPosition));
        }
    }

    /**
     * Cái này ez
     *
     * @return trả về chiều cao của status bar nhưng dường như thằng code quên dùng LOL
     */
    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = mContext.getResources().getIdentifier("status_bar_height", "dimen", "android");

        if (resourceId > 0) {
            result = mContext.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }
}
