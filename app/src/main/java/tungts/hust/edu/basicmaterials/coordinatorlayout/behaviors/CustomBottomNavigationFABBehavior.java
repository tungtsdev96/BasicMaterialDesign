package tungts.hust.edu.basicmaterials.coordinatorlayout.behaviors;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class CustomBottomNavigationFABBehavior extends CoordinatorLayout.Behavior {

    public CustomBottomNavigationFABBehavior() {
    }

    public CustomBottomNavigationFABBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, View child, View dependency) {
        return dependency instanceof Snackbar.SnackbarLayout;
    }

    @Override
    public void onDependentViewRemoved(CoordinatorLayout parent, View child, View dependency) {
        child.animate().translationY(0);
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, View child, View dependency) {
        return update(child, dependency);
    }

    private boolean update(View fab, View snackBarLayout) {
        if (snackBarLayout instanceof Snackbar.SnackbarLayout){
            float oldTranslation =  fab.getTranslationY();
            float height = snackBarLayout.getHeight(); Log.e("scroll height", height+"");
            float newTranslation = snackBarLayout.getTranslationY() - height;
            fab.setTranslationY(newTranslation);
            Log.e("scroll", newTranslation + " " + oldTranslation);
            return oldTranslation != newTranslation;
        }
        return false;
    }
}
