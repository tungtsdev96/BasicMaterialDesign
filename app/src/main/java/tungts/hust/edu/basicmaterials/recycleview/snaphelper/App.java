package tungts.hust.edu.basicmaterials.recycleview.snaphelper;

import java.util.ArrayList;
import java.util.List;

import tungts.hust.edu.basicmaterials.R;

public class App {

    public String name;
    public int drawable;

    public App(String name, int drawable) {
        this.name = name;
        this.drawable = drawable;
    }

    public static List<App> getAppList() {

        List<App> appList = new ArrayList<>();

        appList.add(new App("WhatsApp", R.mipmap.ic_launcher));
        appList.add(new App("Skype", R.mipmap.ic_launcher));
        appList.add(new App("Facebook", R.mipmap.ic_launcher));
        appList.add(new App("Google+", R.mipmap.ic_launcher));
        appList.add(new App("Instagram", R.mipmap.ic_launcher));
        appList.add(new App("LinkedIn", R.mipmap.ic_launcher));
        appList.add(new App("Quora", R.mipmap.ic_launcher));
        appList.add(new App("Twitter", R.mipmap.ic_launcher));
        appList.add(new App("Tumblr", R.mipmap.ic_launcher));
        appList.add(new App("Email", R.mipmap.ic_launcher));
        appList.add(new App("Gallery", R.mipmap.ic_launcher));


        return appList;
    }

}