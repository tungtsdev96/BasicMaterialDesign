package tungts.hust.edu.basicmaterials.recycleview.dragandswipe;

import android.graphics.Color;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import java.util.ArrayList;

import tungts.hust.edu.basicmaterials.R;

public class DragAndSwipeDemo extends AppCompatActivity implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    RecyclerView rcv;
    CardListAdapter cardListAdapter;
    ArrayList<Item> items;
    CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drag_and_swipe_demo);

        coordinatorLayout = findViewById(R.id.coordinatorLayout);
        rcv = findViewById(R.id.rcv);
        items = new ArrayList<>();
        items.add(new Item(1, "Name ", "Des", 3.4, "thum"));
        items.add(new Item(1, "Name ", "Des", 3.4, "thum"));
        items.add(new Item(1, "Name ", "Des", 3.4, "thum"));
        items.add(new Item(1, "Name ", "Des", 3.4, "thum"));
        items.add(new Item(1, "Name ", "Des", 3.4, "thum"));
        items.add(new Item(1, "Name ", "Des", 3.4, "thum"));
        items.add(new Item(1, "Name ", "Des", 3.4, "thum"));
        items.add(new Item(1, "Name ", "Des", 3.4, "thum"));
        items.add(new Item(1, "Name ", "Des", 3.4, "thum"));
        items.add(new Item(1, "Name ", "Des", 3.4, "thum"));
        items.add(new Item(1, "Name ", "Des", 3.4, "thum"));
        items.add(new Item(1, "Name ", "Des", 3.4, "thum"));
        items.add(new Item(1, "Name ", "Des", 3.4, "thum"));
        cardListAdapter = new CardListAdapter(this, items);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rcv.setLayoutManager(mLayoutManager);
        rcv.setItemAnimator(new DefaultItemAnimator());
        rcv.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        rcv.setAdapter(cardListAdapter);

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(rcv);
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof CardListAdapter.MyViewHolder) {
            // get the removed item name to display it in snack bar
            String name = items.get(viewHolder.getAdapterPosition()).getName();

            // backup of removed item for undo purpose
            final Item deletedItem = items.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();

            // remove the item from recycler view
            cardListAdapter.removeItem(viewHolder.getAdapterPosition());

            // showing snack bar with Undo option
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, name + " removed from cart!", Snackbar.LENGTH_LONG);
            snackbar.setAction("UNDO", new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // undo is selected, restore the deleted item
                    cardListAdapter.restoreItem(deletedItem, deletedIndex);
                }
            });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();
        }
    }

    @Override
    public void onItemMove(int fromPositon, int toPositon) {
        cardListAdapter.onItemMove(fromPositon, toPositon);
    }
}
