package tungts.hust.edu.basicmaterials.coordinatorlayout.behaviors;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;

public class CustomBrhaviorBottomNavigationBar extends CoordinatorLayout.Behavior {

    public CustomBrhaviorBottomNavigationBar() {
    }

    public CustomBrhaviorBottomNavigationBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, View child, View dependency) {
        if (dependency instanceof Snackbar.SnackbarLayout){
            updateSnackBar(child, (Snackbar.SnackbarLayout) dependency);
        }
        return dependency instanceof NestedScrollView;
    }

    private void updateSnackBar(View child, Snackbar.SnackbarLayout dependency) {
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) dependency.getLayoutParams();
        params.setAnchorId(child.getId());
        params.anchorGravity = Gravity.TOP;
        params.gravity = Gravity.TOP;
        dependency.setLayoutParams(params);
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, View child, View dependency) {
        return super.onDependentViewChanged(parent, child, dependency);
    }

    @Override
    public boolean onStartNestedScroll(@NonNull CoordinatorLayout coordinatorLayout, @NonNull View child, @NonNull View directTargetChild, @NonNull View target, int axes) {
        return axes == ViewCompat.SCROLL_AXIS_VERTICAL;
    }

    @Override
    public void onNestedPreScroll(@NonNull CoordinatorLayout coordinatorLayout, @NonNull View child, @NonNull View target, int dx, int dy, @NonNull int[] consumed) {
        if (dy < 0){
            showBottomMenu(child);
        } else {
            hideBottomMenu(child);
        }
    }

    private void hideBottomMenu(View child) {
        child.clearAnimation();
        child.animate().translationY(child.getHeight());
    }

    private void showBottomMenu(View child) {
        child.clearAnimation();
        child.animate().translationY(0);
    }
}
