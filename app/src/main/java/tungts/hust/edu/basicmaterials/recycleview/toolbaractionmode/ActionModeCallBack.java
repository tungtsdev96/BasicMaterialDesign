package tungts.hust.edu.basicmaterials.recycleview.toolbaractionmode;

import android.support.v7.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

import tungts.hust.edu.basicmaterials.R;

public class ActionModeCallBack implements ActionMode.Callback {

    //dung activity de call back
    RecycleViewActivity recycleViewActivity;
    public ActionModeCallBack(RecycleViewActivity recycleViewActivity) {
        this.recycleViewActivity = recycleViewActivity;
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        mode.getMenuInflater().inflate(R.menu.contextual_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {

        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_1:
                //delete
                recycleViewActivity.deleteAllItemSelected();
                break;
            case R.id.menu_2:
                recycleViewActivity.selectAllItem();
                //select all
                break;
            case android.R.id.home:
                recycleViewActivity.removeAllSelected();
                recycleViewActivity.setNullToActionMode();
                break;
        }
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        //When action mode destroyed remove selected selections and set action mode to null
        recycleViewActivity.removeAllSelected();
        recycleViewActivity.setNullToActionMode();
    }
}
