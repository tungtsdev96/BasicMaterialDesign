package tungts.hust.edu.basicmaterials.coordinatorlayout.behaviors;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.TextView;

public class CustomBehaviorTouch extends CoordinatorLayout.Behavior<TextView> {

    public CustomBehaviorTouch(){}

    public CustomBehaviorTouch(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(CoordinatorLayout parent, TextView child, MotionEvent ev) {
        Log.e("tungts","onInterceptTouchEvent: " + ev.getAction()+"|"+(int)ev.getX()+"|"+(int)ev.getY());
        child.setText(ev.getAction()+"|"+(int)ev.getX()+"|"+(int)ev.getY());
        return true;
    }

    @Override
    public boolean onTouchEvent(CoordinatorLayout parent, TextView child, MotionEvent ev) {
        Log.e("tungts","onTouchEvent: " + ev.getAction()+"|"+(int)ev.getX()+"|"+(int)ev.getY());
        child.setText(ev.getAction()+"|"+(int)ev.getX()+"|"+(int)ev.getY());
        return true;
    }
}
