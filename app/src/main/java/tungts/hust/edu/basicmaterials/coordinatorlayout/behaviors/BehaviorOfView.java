package tungts.hust.edu.basicmaterials.coordinatorlayout.behaviors;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import tungts.hust.edu.basicmaterials.R;

public class BehaviorOfView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_behavior_of_view);
    }
}
